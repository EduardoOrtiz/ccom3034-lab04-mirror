/*=============================================================================
Nombre: 		Eduardo Ortiz
Numero de Estudiante:	801-11-5100
Correo Electronico:	eduardo3991@hotmail.com
=============================================================================*/


#include "dialog.h"
#include <QtDebug>
#include <cstdlib>		//random numbers
#include <ctime>		//time function

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
{
    CircleWidth = 0;
    CircleHeight = 0;

    GoingRight = true;
    GoingDown = true;

    myTimer = new QTimer(this);

    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));

    myTimer->start(1);
}

bool Dialog::horizontalOk()
{
    return ((CircleWidth >= 0) && (CircleWidth <= width() - 20))? true : false;
}

bool Dialog::verticalOk()
{
    return ((CircleHeight >= 0) && (CircleHeight <= height() - 20))? true : false;
}


Dialog::~Dialog()
{
    delete myTimer;
}

void Dialog::mousePressEvent(QMouseEvent *event)
{
    //out the x, y coordinates and the button press

    if(event->button() == 1 )
        qDebug() << "clicked at: " << event->x() << "," << event->y()
                 << "with the left button.";


    if(event->button() == 2 )
        qDebug() << "clicked at: " << event->x() << "," << event->y()
                 << "with the right button.";
}

void Dialog::paintEvent(QPaintEvent *event)
{
   // qDebug() << "Inside the paint event method!";

    //srand(time(NULL));

    QPainter p(this);
    QPen myPen;

    myPen.setWidth(10);
    myPen.setColor(QColor(0xff0000));
    p.setPen(myPen);

    p.drawEllipse(CircleWidth, CircleHeight, 20, 20);

    CircleRegion = event->region();
}

//I would like to make the circle faster, but while loops are 2fast4me
void Dialog::mySlot()
{
    //qDebug() << "tick";

    if(GoingRight && GoingDown)
    {
        if(horizontalOk())
            CircleWidth++;

        else
        {
            GoingRight = false;
            CircleWidth--;
        }

        if(verticalOk())
          CircleHeight++;

        else
        {
            GoingDown = false;
            CircleHeight--;
        }

        repaint();
    }

    if(GoingRight && !GoingDown)
    {
        if(horizontalOk())
            CircleWidth++;

        else
        {
            GoingRight = false;
            CircleWidth--;
        }

        if(verticalOk())
          CircleHeight--;

        else
        {
            GoingDown = true;
            CircleHeight++;
        }

        repaint();
    }

    if(!GoingRight && GoingDown)
    {
        if(horizontalOk())
            CircleWidth--;

        else
        {
            GoingRight = true;
            CircleWidth++;
        }

        if(verticalOk())
          CircleHeight++;

        else
        {
            GoingDown = false;
            CircleHeight--;
        }

        repaint();
    }

    if(!GoingRight && !GoingDown)
    {
        if(horizontalOk())
            CircleWidth--;

        else
        {
            GoingRight = true;
            CircleWidth++;
        }

        if(verticalOk())
          CircleHeight--;

        else
        {
            GoingDown = true;
            CircleHeight++;
        }

        repaint();
    }
}
