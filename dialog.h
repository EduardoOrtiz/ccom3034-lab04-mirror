/*=============================================================================
Nombre: 		Eduardo Ortiz
Numero de Estudiante:	801-11-5100
Correo Electronico:	eduardo3991@hotmail.com
=============================================================================*/

#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtGui>
#include <QtCore>
#include <QTimer>

class Dialog : public QDialog
{
    Q_OBJECT

public:

    QRegion CircleRegion;
    QTimer *myTimer;

    int CircleWidth;
    int CircleHeight;

    bool GoingRight;
    bool GoingDown;

    Dialog(QWidget *parent = 0);
    ~Dialog();

    bool horizontalOk();
    bool verticalOk();

public slots:
    void mySlot();

protected:
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
};

#endif // DIALOG_H
