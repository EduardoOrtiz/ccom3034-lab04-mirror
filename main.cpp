/*=============================================================================
Nombre: 		Eduardo Ortiz
Numero de Estudiante:	801-11-5100
Correo Electronico:	eduardo3991@hotmail.com
=============================================================================*/
#include "dialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog w;
    w.show();

    return a.exec();
}
